﻿using UnityEngine;

public class SpikeTrigger : MonoBehaviour
{

  [SerializeField]
  int baseDamage = 0;

  void OnTriggerEnter2D(Collider2D other)
  {
    if (other.CompareTag("Player"))
      other.GetComponent<IDamageable>().TakeDamage(baseDamage);
  }

}
