﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Popup : MonoBehaviour
{

  float _moveSpeed;
  float _fadeDelay;
  float _fadeSpeed;
  Color _textColor;
  TextMeshPro textMesh;

  void Awake()
  {
    textMesh = transform.GetComponent<TextMeshPro>();
  }

  // Update is called once per frame
  void Update()
  {
    Move();
    Fade();
  }

  public void Setup(float moveSpeed, float fadeDelay, float fadeSpeed, string text)
  {
    _moveSpeed = moveSpeed;
    _fadeDelay = fadeDelay;
    _fadeSpeed = fadeSpeed;
    textMesh.text = text;
    _textColor = textMesh.color;
  }

  void Move()
  {
    transform.position += new Vector3(0, _moveSpeed) * Time.deltaTime;
  }

  void Fade()
  {
    _fadeDelay -= Time.deltaTime;
    if (_fadeDelay <= 0)
    {
      _textColor.a -= _fadeSpeed * Time.deltaTime;
      textMesh.color = _textColor;
      if (_textColor.a <= 0)
        Destroy(gameObject);
    }
  }
}
