﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
  [SerializeField]
  Rigidbody2D rb = null;

  [SerializeField]
  Transform body = null;

  [SerializeField]
  float speed = 0f;

  void Start()
  {
    Move();
  }

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.CompareTag("Wall"))
      Flip();
  }

  public void Move(int directionModifier = 1, float speedModifier = 1)
  {
    rb.velocity = (-Vector2.right * directionModifier) * (speed + speedModifier);
  }

  public float Stop()
  {
    var direction = rb.velocity.x;
    rb.velocity = Vector2.zero;
    return direction;
  }

  void Flip()
  {
    Debug.Log("Wall");
    FlipSprite();
    FlipMovement();
  }

  void FlipSprite()
  {
    if (rb.velocity.x < 0)
      body.localScale = new Vector3(-1f, 1f, 0f);
    else if (rb.velocity.x > 0)
      body.localScale = new Vector3(1f, 1f, 0f);
  }

  void FlipMovement()
  {
    if (rb.velocity.x < 0)
      Move(-1);
    else
      Move();
  }
}
