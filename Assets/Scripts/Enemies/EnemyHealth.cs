﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour, IDamageable
{

  [SerializeField]
  GameObject popupPrefab = null;

  [SerializeField]
  float moveSpeed = 0f;

  [SerializeField]
  float fadeDelay = 0f;

  [SerializeField]
  float fadeSpeed = 0f;

  [SerializeField]
  int startingHealth = 0;

  [SerializeField]
  float baseDamageCooldown = 0f;

  float _damageCooldown = 0f;

  int _currentHealth;

  void Awake()
  {
    _currentHealth = startingHealth;
  }

  void Update()
  {
    DamageCooldown();
    CheckIfDead();
  }

  void DamageCooldown()
  {
    if (_damageCooldown > 0)
      _damageCooldown -= Time.deltaTime;
  }

  public void TakeDamage(int amount)
  {
    if (_damageCooldown <= 0)
    {
      _currentHealth -= amount;
      var popup = Instantiate(popupPrefab, new Vector2(transform.position.x, transform.position.y + 1), Quaternion.identity);
      popup.GetComponent<Popup>().Setup(moveSpeed, fadeDelay, fadeSpeed, "-" + amount.ToString());
      _damageCooldown = baseDamageCooldown;
    }
  }

  public void CheckIfDead()
  {
    if (_currentHealth <= 0)
      Die();
  }

  public void Die()
  {
    Destroy(gameObject);
  }

}
