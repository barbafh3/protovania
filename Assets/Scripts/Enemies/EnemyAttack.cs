﻿using UnityEngine;

public class EnemyAttack : MonoBehaviour, IMeleeAttacker
{

  [SerializeField]
  EnemyMovement enemyMovement = null;

  [SerializeField]
  float dashDuration = 0f;

  [SerializeField]
  float recoveryTime = 0f;

  [SerializeField]
  float speedModifier = 0f;

  float direction = 0f;

  bool isAttacking = false;

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.CompareTag("Player") && !isAttacking)
      Attack();
  }

  void Attack()
  {
    isAttacking = true;
    direction = enemyMovement.Stop();
    Invoke("MeleeAttack", 1f);
  }

  public void MeleeAttack()
  {
    Debug.Log("Attacked");
    if (direction < 0f)
      enemyMovement.Move(1, speedModifier);
    else if (direction > 0f)
      enemyMovement.Move(-1, speedModifier);

    Invoke("ReturnToIdle", dashDuration);
  }

  void ReturnToIdle()
  {
    enemyMovement.Stop();
    Invoke("ResumeMovement", recoveryTime);
  }

  void ResumeMovement()
  {
    if (direction < 0f)
      enemyMovement.Move();
    else if (direction > 0f)
      enemyMovement.Move(-1);
    direction = 0f;
    isAttacking = false;
  }
}
