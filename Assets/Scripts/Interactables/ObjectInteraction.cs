using UnityEngine;

public class ObjectInteraction : MonoBehaviour, IInteractable
{

  [SerializeField]
  GameObject interactSign = null;

  Color _baseColor;

  SpriteRenderer _signSpriteRenderer;

  void Awake()
  {
    ToggleInteractSign(false);
    _signSpriteRenderer = interactSign.GetComponent<SpriteRenderer>();
    _baseColor = _signSpriteRenderer.color;
  }

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.CompareTag("Player"))
      ToggleInteractSign(true);
  }

  private void OnTriggerExit2D(Collider2D other)
  {
    if (other.CompareTag("Player"))
      ToggleInteractSign(false);
  }

  public void InteractResponse()
  {
    _signSpriteRenderer.color = new Color(186f / 255f, 46f / 255f, 46f / 255f);
    Invoke("ResetSignColor", 0.5f);
  }

  void ResetSignColor()
  {
    _signSpriteRenderer.color = _baseColor;
  }

  public void ToggleInteractSign(bool state)
  {
    interactSign.SetActive(state);
  }
}