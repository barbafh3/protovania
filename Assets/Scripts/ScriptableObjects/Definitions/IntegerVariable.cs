using UnityEngine;

[CreateAssetMenu(fileName = "New Integer Variable", menuName = "Variables/Integer Variable")]
public class IntegerVariable : ScriptableObject
{
  public int Value;
}