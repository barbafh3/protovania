﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraShakeEventListener : MonoBehaviour
{
  public CameraShakeEvent Event;
  public CameraShakeEventParams Response;

  private void OnEnable()
  {
    Event.RegisterListener(this);
  }

  private void OnDisable()
  {
    Event.UnregisterListener(this);
  }

  public void OnEventRaised(float arg1, float arg2, float arg3)
  {
    Response.Invoke(arg1, arg2, arg3);
  }
}
