﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CameraShakeEventParams : UnityEvent<float, float, float> { }

[CreateAssetMenu(fileName = "New Event", menuName = "Events/Camera Shake")]
public class CameraShakeEvent : ScriptableObject
{
  private List<CameraShakeEventListener> listeners = new List<CameraShakeEventListener>();

  public void Raise(float arg1, float arg2, float arg3)
  {
    for (int i = listeners.Count - 1; i >= 0; i--)
    {
      listeners[i].OnEventRaised(arg1, arg2, arg3);
    }
  }

  public void RegisterListener(CameraShakeEventListener listener)
  {
    if (!listeners.Contains(listener))
    {
      listeners.Add(listener);
    }
  }

  public void UnregisterListener(CameraShakeEventListener listener)
  {
    if (listeners.Contains(listener))
    {
      listeners.Remove(listener);
    }
  }

}
