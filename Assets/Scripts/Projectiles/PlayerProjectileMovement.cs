﻿using System.Collections;
using UnityEngine;

public class PlayerProjectileMovement : MonoBehaviour
{

  [SerializeField]
  Rigidbody2D rb = null;

  [SerializeField]
  Animator anim = null;

  [SerializeField]
  float animationLength = 0f;

  [SerializeField]
  float speed = 0f;

  [SerializeField]
  float duration = 0f;

  [HideInInspector]
  public float direction = 0f;

  float _scaleX;

  PlayerProjectileEffects projectileEffects;

  void Awake()
  {
    projectileEffects = GetComponent<PlayerProjectileEffects>();
  }

  void Start()
  {
    _scaleX = transform.localScale.x;
    rb.velocity = new Vector2(direction * speed, rb.velocity.y);
  }

  void Update()
  {
    Flip();
    duration -= Time.deltaTime;
    if (duration <= 0f)
      Expire();
  }

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.CompareTag("Enemy"))
    {
      other.GetComponent<IDamageable>().TakeDamage(1);
      StartCoroutine(Collided());
    }
    if (other.CompareTag("Ground"))
      StartCoroutine(Collided());
  }

  void Flip()
  {
    if (rb.velocity.x > 0)
      transform.localScale = new Vector3(_scaleX, 1f, 1f);
    else if (rb.velocity.x < 0)
      transform.localScale = new Vector3(_scaleX * -1f, 1f, 1f);
  }

  IEnumerator Collided()
  {
    rb.velocity = Vector2.zero;
    var spriteRenderer = GetComponent<SpriteRenderer>();
    projectileEffects.CallCameraShake();
    projectileEffects.CallCollideSound();
    anim.Play("PlayerProjectileHit");
    yield return new WaitForSeconds(animationLength);
    // yield return new WaitForSeconds(0.2f);
    Die();
  }

  void Expire()
  {
    // Expire animation
    Die();
  }

  void Die()
  {
    Destroy(gameObject);
  }
}
