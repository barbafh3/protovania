using UnityEngine;

public class PlayerProjectileEffects : MonoBehaviour
{

  // Camera shake event
  [SerializeField]
  CameraShakeEvent shakeEvent = null;

  [SerializeField]
  AudioSource explosionSound = null;

  // Camera shake parameters
  [SerializeField]
  float shakeDuration = 0f;
  [SerializeField]
  float shakeAmplitude = 0f;
  [SerializeField]
  float shakeFrequency = 0f;

  public void CallCameraShake()
  {
    shakeEvent.Raise(shakeDuration, shakeAmplitude, shakeFrequency);
  }

  public void CallCollideSound()
  {
    explosionSound.Play();
  }

}