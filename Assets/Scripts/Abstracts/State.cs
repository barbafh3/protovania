using UnityEngine;

public abstract class State
{

  public abstract void EnterState(PlayerMovement player);
  public abstract void Update(PlayerMovement player);
  public virtual void FixedUpdate(PlayerMovement player) { }
  public virtual void OnCollisionEnter2D(PlayerMovement player, Collision2D other) { }
  public virtual void OnCollisionStay2D(PlayerMovement player, Collision2D other) { }
  public virtual void OnCollisionExit2D(PlayerMovement player, Collision2D other) { }
  public virtual void OnTriggerEnter2D(PlayerMovement player, Collider2D other) { }
  public virtual void OnTriggerStay2D(PlayerMovement player, Collider2D other) { }
  public virtual void OnTriggerExit2D(PlayerMovement player, Collider2D other) { }

}