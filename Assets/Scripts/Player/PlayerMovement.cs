﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

  public Rigidbody2D rb = null;
  public Animator anim = null;
  public LayerMask raycastLayerMask = 0;

  public AudioSource landingSound = null;
  public AudioSource jumpSound = null;
  public AudioSource dashSound = null;

  public float dashDistance = 0f;

  public float baseMoveSpeed = 0f;
  [HideInInspector] public float moveSpeed = 0f;

  public float jumpHeight = 0f;
  public int baseJumpCount = 0;
  [HideInInspector] public int jumpCount = 0;

  public float climbSpeed = 0f;

  public float faceDirection = 1f;
  public bool canMove { get; private set; } = true;

  public State currentState;

  void Awake()
  {
    ChangeState(new PlayerIdleState());
  }

  void Update()
  {
    Flip(transform.localScale.x);
    currentState.Update(this);
  }

  public void ChangeState(State state)
  {
    if (currentState?.GetType() != state?.GetType())
      Debug.Log("Current state: " + currentState?.GetType() + " - New State: " + state?.GetType());
    currentState = state;
    currentState.EnterState(this);
  }

  private void OnCollisionEnter2D(Collision2D other)
  {
    currentState.OnCollisionEnter2D(this, other);
  }

  public void Flip(float scale)
  {
    if (faceDirection > 0)
      transform.localScale = new Vector2(Mathf.Abs(scale), Mathf.Abs(scale));
    else if (faceDirection < 0)
      transform.localScale = new Vector2(-Mathf.Abs(scale), Mathf.Abs(scale));
  }

}
