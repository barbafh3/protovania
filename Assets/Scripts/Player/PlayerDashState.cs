using UnityEngine;

public class PlayerDashState : State
{
  public override void EnterState(PlayerMovement player)
  {
    player.anim.SetBool("Climbing", false);
    player.dashSound.Play();
    player.rb.AddForce(Vector2.right * player.faceDirection * player.dashDistance, 0f);
  }

  public override void Update(PlayerMovement player)
  {
    player.ChangeState(new PlayerIdleState());
  }
}