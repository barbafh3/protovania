using UnityEngine;

public class PlayerAttack : MonoBehaviour, IMeleeAttacker
{

  [SerializeField]
  float baseCooldown = 0f;

  float _cooldown = 0f;

  AudioSource _attackSound;
  BoxCollider2D _boxCollider;
  Animator _animator;


  void Awake()
  {
    _attackSound = GetComponent<AudioSource>();
    _boxCollider = GetComponent<BoxCollider2D>();
    _animator = transform.parent.GetComponent<Animator>();
    DisableAttack();
  }

  void Start()
  {
    _boxCollider.enabled = false;
  }

  void Update()
  {
    CooldownTick();
    Attack();
  }

  public void Attack()
  {
    if (Input.GetKeyDown(KeyCode.K) && _cooldown <= 0f)
    {
      MeleeAttack();
    }
  }

  void CooldownTick()
  {
    if (_cooldown > 0f)
      _cooldown -= Time.deltaTime;
  }

  public void MeleeAttack()
  {
    _animator.SetTrigger("Attack");
    _attackSound.Play();
    Debug.Log("Player attack animation triggered");
    _boxCollider.enabled = true;
    Invoke("DisableAttack", 0.1f);
    _cooldown = baseCooldown;
  }

  void DisableAttack()
  {
    _boxCollider.enabled = false;
  }
}