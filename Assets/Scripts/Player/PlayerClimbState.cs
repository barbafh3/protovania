using UnityEngine;

public class PlayerClimbState : State
{

  public override void EnterState(PlayerMovement player)
  {
    if (!CheckForWalls(player)) player.ChangeState(new PlayerIdleState());
    else
    {
      player.anim.SetBool("Jumping", false);
      player.anim.SetBool("Falling", false);
    }
  }

  public override void Update(PlayerMovement player)
  {
    if (CheckForWalls(player))
    {
      if (Input.GetKeyDown(KeyCode.C))
        player.ChangeState(new PlayerDashState());

      if (Input.GetKeyDown(KeyCode.Space) && player.jumpCount > 0)
        player.ChangeState(new PlayerJumpState());

      var vertical = Input.GetAxisRaw("Vertical");
      player.rb.velocity = new Vector2(player.rb.velocity.x, vertical * player.climbSpeed);
      player.anim.SetBool("Climbing", true);
      player.anim.SetFloat("Speed", 1);
    }
    else
    {
      player.anim.SetBool("Climbing", false);
      player.anim.SetFloat("Speed", 0);
      player.ChangeState(new PlayerIdleState());
    }
  }

  bool CheckForWalls(PlayerMovement player)
  {
    if (LeftSideCollision(player) || RightSideCollision(player)) return true;
    else return false;
  }

  public bool LeftSideCollision(PlayerMovement player)
  {
    bool result = Physics2D.OverlapArea(new Vector2(player.transform.position.x - 0.4f, player.transform.position.y + 0.25f),
              new Vector2(player.transform.position.x - 0.6f, player.transform.position.y - 0.25f),
              player.raycastLayerMask);
    if (result) player.faceDirection = 1f;
    return result;
  }

  public bool RightSideCollision(PlayerMovement player)
  {
    bool result = Physics2D.OverlapArea(new Vector2(player.transform.position.x + 0.4f, player.transform.position.y + 0.25f),
              new Vector2(player.transform.position.x + 0.6f, player.transform.position.y - 0.25f),
              player.raycastLayerMask);
    if (result) player.faceDirection = -1f;
    return result;
  }

  private void OnDrawGizmos(PlayerMovement player)
  {
    Gizmos.color = Color.yellow;
    Gizmos.DrawCube(new Vector2(player.transform.position.x - 0.405f, player.transform.position.y),
            new Vector2(0.01f, 0.5f));
    Gizmos.DrawCube(new Vector2(player.transform.position.x + 0.405f, player.transform.position.y),
            new Vector2(0.01f, 0.5f));
    Gizmos.color = Color.red;
    Gizmos.DrawCube(new Vector2(player.transform.position.x, player.transform.position.y - 0.505f),
            new Vector2(1, 0.01f));
  }

}