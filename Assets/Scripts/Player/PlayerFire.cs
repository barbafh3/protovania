﻿using UnityEngine;

public class PlayerFire : MonoBehaviour, IRangedAttacker
{

  [SerializeField] GameObject projectilePrefab = null;
  [SerializeField] PlayerMovement playerMovement = null;
  [SerializeField] Animator anim = null;
  [SerializeField] Transform projectileOrigin = null;
  [SerializeField] AudioSource fireSound = null;

  [SerializeField] float baseCooldown = 0f;

  float _cooldown = 0f;


  void Awake()
  {
    _cooldown = baseCooldown;
  }

  void Update()
  {
    DecreaseCooldown();

    if (Input.GetKeyDown(KeyCode.F) && _cooldown == 0f)
      RangedAttack();
  }

  private void DecreaseCooldown()
  {
    if (_cooldown > 0f)
    {
      _cooldown -= Time.deltaTime;
      if (_cooldown < 0f)
        _cooldown = 0f;
    }
  }

  public void RangedAttack()
  {
    var projectile = Instantiate(projectilePrefab, projectileOrigin.position, Quaternion.identity);
    projectile.GetComponent<PlayerProjectileMovement>().direction = playerMovement.faceDirection;
    _cooldown = baseCooldown;
    anim.SetTrigger("Fire");
    fireSound.Play();
  }
}
