﻿using UnityEngine;

public class PlayerDash : MonoBehaviour
{

  [SerializeField]
  Camera cam = null;

  [SerializeField]
  Rigidbody2D rb = null;

  [SerializeField]
  PlayerMovement playerMovement = null;

  [SerializeField]
  AudioSource dashSound = null;

  [SerializeField]
  float dashDistance = 0f;

  [SerializeField]
  float baseCooldown = 0f;

  [HideInInspector]
  public bool isAxisDashEnabled = false;

  private float _cooldown;

  void Awake()
  {
    ResetCooldown();
  }

  void Update()
  {
    CooldownTick();
    var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    if (Input.GetKeyDown(KeyCode.C) && _cooldown <= 0f)
    {
      ChooseDash();
      //   Debug.Log($"X: {mousePos.x - transform.position.x}");
      //   Debug.Log($"Y: {mousePos.y - transform.position.y}");
      //   Debug.Log($"Direction: {mousePos - transform.position}");
    }
  }

  private void CooldownTick()
  {
    if (_cooldown > 0)
      _cooldown -= Time.deltaTime;
  }

  private void ResetCooldown()
  {
    _cooldown = baseCooldown;
  }

  void ChooseDash()
  {
    if (isAxisDashEnabled)
      AxisDash();
    else
      HorizontalDash();
  }

  void HorizontalDash()
  {
    dashSound.Play();
    rb.AddForce(Vector2.right * playerMovement.faceDirection * dashDistance, 0f);
    ResetCooldown();
  }

  void AxisDash()
  {
    dashSound.Play();
    ResetCooldown();
  }

  void StopDash()
  {
    rb.velocity = Vector2.zero;
  }

}
