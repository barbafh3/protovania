using UnityEngine;

public class PlayerMoveState : State
{
  public override void EnterState(PlayerMovement player) { }

  public override void Update(PlayerMovement player)
  {
    if (Input.GetKeyDown(KeyCode.C)) player.ChangeState(new PlayerDashState());

    if (player.rb.velocity.y < -0.1) player.ChangeState(new PlayerFallState());

    if (Input.GetKeyDown(KeyCode.Space) && player.jumpCount > 0)
      player.ChangeState(new PlayerJumpState());

    var horizontal = Input.GetAxisRaw("Horizontal");
    if (horizontal != 0) player.faceDirection = horizontal;
    player.anim.SetFloat("Horizontal", Mathf.Abs(horizontal));
    player.anim.SetFloat("Speed", 1f);
    player.rb.velocity = new Vector2(horizontal * player.moveSpeed, player.rb.velocity.y);

    if (player.rb.velocity.x == 0)
    {
      player.anim.SetFloat("Speed", 0f);
      player.ChangeState(new PlayerIdleState());
    }

    if (Input.GetAxisRaw("Vertical") != 0)
      player.ChangeState(new PlayerClimbState());
  }
}