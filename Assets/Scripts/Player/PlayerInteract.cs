using UnityEngine;

public class PlayerInteract : MonoBehaviour
{

  private void OnTriggerStay2D(Collider2D other)
  {
    if (other.CompareTag("Interactable"))
    {
      if (Input.GetKeyDown(KeyCode.E))
        other.GetComponent<IInteractable>().InteractResponse();
    }

  }

}