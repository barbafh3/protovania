using UnityEngine;

public class PlayerHealth : MonoBehaviour, IDamageable
{
  [SerializeField]
  int startingHealth = 0;

  [SerializeField]
  int maxHealth = 0;

  [SerializeField]
  IntegerVariable currentHealth = null;

  void Awake()
  {
    currentHealth.Value = startingHealth;
  }

  void Update()
  {
    CheckIfDead();
  }

  public void IncreaseHealth(int amount)
  {
    if (currentHealth.Value + amount >= maxHealth)
      currentHealth.Value = maxHealth;
    else
      currentHealth.Value += amount;
  }

  public void TakeDamage(int amount)
  {
    currentHealth.Value -= amount;
    // TODO hit taken animation
  }

  public void CheckIfDead()
  {
    if (currentHealth.Value <= 0)
    {
      Die();
    }
  }

  public void Die()
  {
    // TODO death animation
    // TODO any other action before death
  }

}