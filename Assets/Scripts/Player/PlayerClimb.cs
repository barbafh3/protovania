﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClimb : MonoBehaviour
{
  [SerializeField] LayerMask raycastLayerMask = 0;
  [SerializeField] Rigidbody2D rb = null;
  [SerializeField] Animator animator = null;
  [SerializeField] PlayerMovement playerMovement = null;

  [SerializeField] float climbSpeed = 0f;

  public bool isOnWall = false;

  float _side;

  void Update()
  {
    isOnWall = CheckForWalls();
    if (isOnWall)
    {
      Climb();
    }
    else
    {
      animator.SetBool("Climbing", false);
      animator.SetFloat("Speed", 0);
    }
    // playerMovement.Flip(_side);
  }

  void Climb()
  {
    var vertical = Input.GetAxisRaw("Vertical");
    rb.velocity = new Vector2(rb.velocity.x, vertical * climbSpeed);
    animator.SetBool("Climbing", true);
    animator.SetFloat("Speed", 1);
  }

  bool CheckForWalls()
  {
    if (LeftSideCollision() || RightSideCollision()) return true;
    else return false;
  }

  public bool LeftSideCollision()
  {
    bool result = Physics2D.OverlapArea(new Vector2(transform.position.x - 0.4f, transform.position.y + 0.25f),
              new Vector2(transform.position.x - 0.6f, transform.position.y - 0.25f),
              raycastLayerMask);
    if (result) _side = 1f;
    return result;
  }

  public bool RightSideCollision()
  {
    bool result = Physics2D.OverlapArea(new Vector2(transform.position.x + 0.4f, transform.position.y + 0.25f),
              new Vector2(transform.position.x + 0.6f, transform.position.y - 0.25f),
              raycastLayerMask);
    if (result) _side = -1f;
    return result;
  }

  private void OnDrawGizmos()
  {
    Gizmos.color = Color.yellow;
    Gizmos.DrawCube(new Vector2(transform.position.x - 0.405f, transform.position.y),
            new Vector2(0.01f, 0.5f));
    Gizmos.DrawCube(new Vector2(transform.position.x + 0.405f, transform.position.y),
            new Vector2(0.01f, 0.5f));
    Gizmos.color = Color.red;
    Gizmos.DrawCube(new Vector2(transform.position.x, transform.position.y - 0.505f),
            new Vector2(1, 0.01f));
  }

}
