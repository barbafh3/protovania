using UnityEngine;

public class PlayerFallState : State
{
  public override void EnterState(PlayerMovement player)
  {
    player.anim.SetBool("Jumping", false);
    player.anim.SetBool("Climbing", false);
    player.anim.SetBool("Falling", true);
  }

  public override void Update(PlayerMovement player)
  {
    var horizontal = Input.GetAxisRaw("Horizontal");
    if (horizontal != 0) player.faceDirection = horizontal;
    player.rb.velocity = new Vector2(horizontal * player.moveSpeed, player.rb.velocity.y);

    if (Input.GetKeyDown(KeyCode.C)) player.ChangeState(new PlayerDashState());

    if (Input.GetKeyDown(KeyCode.Space) && player.jumpCount > 0)
      player.ChangeState(new PlayerJumpState());
  }

  public override void OnCollisionEnter2D(PlayerMovement player, Collision2D other)
  {
    if (other.gameObject.CompareTag("Ground"))
    {
      player.anim.SetBool("Falling", false);
      player.ChangeState(new PlayerIdleState());
    }
  }
}