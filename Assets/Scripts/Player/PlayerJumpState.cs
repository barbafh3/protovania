using UnityEngine;

public class PlayerJumpState : PlayerMoveState
{

  public override void EnterState(PlayerMovement player)
  {
    player.jumpCount--;
    player.anim.SetBool("Jumping", true);
    player.anim.SetBool("Climbing", false);
    player.rb.velocity = Vector2.zero;
    player.jumpSound.Play();
    var horizontal = Input.GetAxisRaw("Horizontal");
    if (horizontal != 0) player.faceDirection = horizontal;
    player.rb.velocity = new Vector2(horizontal * player.moveSpeed, player.rb.velocity.y);
    player.rb.AddForce(Vector2.up * player.jumpHeight);
  }

  public override void Update(PlayerMovement player)
  {
    if (Input.GetKeyDown(KeyCode.C)) player.ChangeState(new PlayerDashState());

    var horizontal = Input.GetAxisRaw("Horizontal");
    if (horizontal != 0) player.faceDirection = horizontal;
    player.rb.velocity = new Vector2(horizontal * player.moveSpeed, player.rb.velocity.y);
    if (player.rb.velocity.y <= 0)
      player.ChangeState(new PlayerFallState());
    if (Input.GetKeyDown(KeyCode.Space) && player.jumpCount > 0)
      EnterState(player);
    if (Input.GetAxisRaw("Vertical") != 0)
      player.ChangeState(new PlayerClimbState());
  }

  public override void OnCollisionEnter2D(PlayerMovement player, Collision2D other)
  {
    if (other.gameObject.CompareTag("Ground"))
      player.ChangeState(new PlayerIdleState());
  }

}