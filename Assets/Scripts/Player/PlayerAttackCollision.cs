﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackCollision : MonoBehaviour
{

  // Camera shake event
  [SerializeField]
  CameraShakeEvent shakeEvent = null;

  // Camera shake parameters
  [SerializeField]
  float shakeDuration = 0f;
  [SerializeField]
  float shakeAmplitude = 0f;
  [SerializeField]
  float shakeFrequency = 0f;

  void OnTriggerEnter2D(Collider2D other)
  {
    if (other.CompareTag("Enemy"))
    {
      other.GetComponent<IDamageable>().TakeDamage(1);
      shakeEvent.Raise(shakeDuration, shakeAmplitude, shakeFrequency);
    }
  }

}
