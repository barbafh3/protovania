using UnityEngine;

public class PlayerIdleState : State
{
  public override void EnterState(PlayerMovement player)
  {
    player.rb.velocity = Vector2.zero;
    player.jumpCount = player.baseJumpCount;
    player.moveSpeed = player.baseMoveSpeed;
  }

  public override void Update(PlayerMovement player)
  {
    if (Input.GetKeyDown(KeyCode.C)) player.ChangeState(new PlayerDashState());

    if (Input.GetKeyDown(KeyCode.Space) && player.jumpCount > 0)
      player.ChangeState(new PlayerJumpState());

    if (Input.GetAxisRaw("Horizontal") != 0) player.ChangeState(new PlayerMoveState());

    if (player.rb.velocity.y < 0) player.ChangeState(new PlayerFallState());

    if (Input.GetAxisRaw("Vertical") != 0 && Input.GetAxisRaw("Horizontal") == 0)
      player.ChangeState(new PlayerClimbState());
  }
}