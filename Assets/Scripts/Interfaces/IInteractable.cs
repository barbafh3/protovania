public interface IInteractable
{
  void InteractResponse();
  void ToggleInteractSign(bool state);
}