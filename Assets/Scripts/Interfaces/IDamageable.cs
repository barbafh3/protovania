public interface IDamageable
{
  void TakeDamage(int amount);
  void CheckIfDead();
  void Die();
}