﻿using UnityEngine;
using Cinemachine;

public class CameraSwap : MonoBehaviour
{

  [SerializeField]
  CinemachineBrain brain = null;

  [SerializeField]
  CinemachineVirtualCamera cam = null;

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.CompareTag("Player"))
      SwapCamera();
  }

  void SwapCamera()
  {
    var currentCam = brain.ActiveVirtualCamera;
    currentCam.VirtualCameraGameObject.SetActive(false);
    cam.gameObject.SetActive(true);
  }
}
