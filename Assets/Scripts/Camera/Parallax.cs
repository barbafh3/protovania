using UnityEngine;

public class Parallax : MonoBehaviour
{

  Transform cameraTransform;

  Vector3 lastCameraPosition;

  [SerializeField]
  float parallaxModifier = 0f;

  void Start()
  {
    cameraTransform = Camera.main.transform;
    lastCameraPosition = cameraTransform.position;
  }

  void FixedUpdate()
  {
    var deltaMovement = cameraTransform.position - lastCameraPosition;
    transform.position += deltaMovement * parallaxModifier;
    lastCameraPosition = cameraTransform.position;
  }

}