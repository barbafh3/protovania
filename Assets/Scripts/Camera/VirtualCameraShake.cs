using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

public class VirtualCameraShake : MonoBehaviour
{

  float _shakeDuration = 0f;          // Time the Camera Shake effect will last
  float _shakeAmplitude = 0f;         // Cinemachine Noise Profile Parameter
  float _shakeFrequency = 0f;         // Cinemachine Noise Profile Parameter

  float _shakeElapsedTime = 0f;

  // Cinemachine Shake
  [SerializeField]
  CinemachineVirtualCamera virtualCamera = null;

  CinemachineBasicMultiChannelPerlin _virtualCameraNoise;

  // Use this for initialization
  void Start()
  {
    // Get Virtual Camera Noise Profile
    if (virtualCamera != null)
      _virtualCameraNoise = virtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
  }

  // Update is called once per frame
  void Update()
  {
    Shake();
  }

  public void StartShake(float duration, float amplitude, float frequency)
  {
    _shakeAmplitude = amplitude;
    _shakeFrequency = frequency;
    _shakeDuration = duration;
    _shakeElapsedTime = _shakeDuration;
  }

  private void Shake()
  {
    // If the Cinemachine componet is not set, avoid update
    if (virtualCamera != null && _virtualCameraNoise != null)
    {
      // If Camera Shake effect is still playing
      if (_shakeElapsedTime > 0)
      {
        // Set Cinemachine Camera Noise parameters
        _virtualCameraNoise.m_AmplitudeGain = _shakeAmplitude;
        _virtualCameraNoise.m_FrequencyGain = _shakeFrequency;

        // Update Shake Timer
        _shakeElapsedTime -= Time.deltaTime;
      }
      else
      {
        // If Camera Shake effect is over, reset variables
        _virtualCameraNoise.m_AmplitudeGain = 0f;
        _shakeElapsedTime = 0f;
      }
    }
  }
}
